﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public GameObject LoadPanel;

    public void Start()
    {
        LoadPanel.SetActive(false);
    }

    public void LoadGameScene()
    {
        LoadPanel.SetActive(true);
        SceneManager.LoadSceneAsync("GameScene");
    }
}
