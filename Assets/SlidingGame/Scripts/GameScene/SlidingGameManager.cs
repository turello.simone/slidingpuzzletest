﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class SlidingGameManager : MonoBehaviour
{
    #region Variables

    public int GridSideSize = 4;
    public GameObject TilePrefab;

    private List<Tile> _tiles; //list of the tiles
    private int _emptyIndex; //index of the empty tile
    private Tile _interactedTile; //tile being iteracted by user input

    public GameObject VictoryPanel;
    private bool _gameWon = false;

    public int NumberOfMoves
    {
        get { return _numberOfMoves; }
        set
        {
            _numberOfMoves = value;
            MovesText.text = value.ToString();
        }
    }
    private int _numberOfMoves;
    public TextMeshProUGUI MovesText;

    public float Timer
    {
        get { return _timer; }
        set
        {
            _timer = value;
            TimerText.text = String.Format("{0:F1} s", value);
        }
    }
    private float _timer;
    public TextMeshProUGUI TimerText;

    #endregion

    #region Unity methods

    private void Start()
    {
        _tiles = new List<Tile>();

        //Instantiate the tiles
        for (int i = 0; i < (GridSideSize * GridSideSize) - 1; i++)
        {
            Tile tile = GameObject.Instantiate(TilePrefab).GetComponent<Tile>();
            _tiles.Add(tile);
        }

        _emptyIndex = (GridSideSize * GridSideSize) - 1;

        //Make sure grid is rendering on screen
        if(Camera.main.aspect < 1)
            Camera.main.orthographicSize = (GridSideSize + 2) * 0.5f / Camera.main.aspect;
        else
            Camera.main.orthographicSize = (GridSideSize + 2) * 0.5f;

        InitializeGrid();
    }

    private void Update()
    {
        if (_gameWon) return;

        Timer += Time.deltaTime;


        //Handle inputs

#if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0))
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);
            if (hit.collider != null)
            {
                _interactedTile = hit.transform.gameObject.GetComponent<Tile>();
                if (TileCanMove(_interactedTile)) _interactedTile.State = Tile.TileState.Active;
                else _interactedTile.State = Tile.TileState.CantMove;
            }
        }
        else if (Input.GetMouseButton(0))
        {
            if (_interactedTile != null && _interactedTile.State == Tile.TileState.Active)
            {
                Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Vector2 tilePos = IndexToPosition(_interactedTile.Index);
                Vector2 emptyPos = IndexToPosition(_emptyIndex);

                //limit movement towards the empty space
                _interactedTile.transform.position =
                    new Vector3(
                        Mathf.Clamp(mousePos.x, Mathf.Min(tilePos.x, emptyPos.x), Mathf.Max(tilePos.x, emptyPos.x)),
                        Mathf.Clamp(mousePos.y, Mathf.Min(tilePos.y, emptyPos.y), Mathf.Max(tilePos.y, emptyPos.y)),
                        0);
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (_interactedTile != null)
            {
                _interactedTile.State = Tile.TileState.Idle;

                //Check if I'm releasing touch when I'm closer to the empty space than the starting position
                if (Vector2.Distance(_interactedTile.transform.position, IndexToPosition(_emptyIndex)) < 0.5f)
                {
                    MoveTile(_interactedTile);
                }
                //reset position
                _interactedTile.transform.position = IndexToPosition(_interactedTile.Index);

                _interactedTile = null;
            }
        }


#elif (UNITY_ANDROID || UNITY_IOS)

        // Handle screen touches.
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            Vector2 touchPos;
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    touchPos = Camera.main.ScreenToWorldPoint(touch.position);
                    RaycastHit2D hit = Physics2D.Raycast(touchPos, Vector2.zero);
                    if (hit.collider != null)
                    {
                        _interactedTile = hit.transform.gameObject.GetComponent<Tile>();
                        if (TileCanMove(_interactedTile)) _interactedTile.State = Tile.TileState.Active;
                        else _interactedTile.State = Tile.TileState.CantMove;
                    }
                    break;
                case TouchPhase.Moved:
                    if (_interactedTile != null && _interactedTile.State == Tile.TileState.Active)
                    {
                        touchPos = Camera.main.ScreenToWorldPoint(touch.position);
                        Vector2 tilePos = IndexToPosition(_interactedTile.Index);
                        Vector2 emptyPos = IndexToPosition(_emptyIndex);

                        //limit movement towards the empty space
                        _interactedTile.transform.position =
                            new Vector3(
                                Mathf.Clamp(touchPos.x, Mathf.Min(tilePos.x, emptyPos.x), Mathf.Max(tilePos.x, emptyPos.x)),
                                Mathf.Clamp(touchPos.y, Mathf.Min(tilePos.y, emptyPos.y), Mathf.Max(tilePos.y, emptyPos.y)),
                                0);
                    }
                    break;
                case TouchPhase.Ended:
                case TouchPhase.Canceled:
                    if (_interactedTile != null)
                    {
                        _interactedTile.State = Tile.TileState.Idle;

                        //Check if I'm releasing touch when I'm closer to the empty space than the starting position
                        if (Vector2.Distance(_interactedTile.transform.position, IndexToPosition(_emptyIndex)) < 0.5f)
                        {
                            MoveTile(_interactedTile);
                        }
                        //reset position
                        _interactedTile.transform.position = IndexToPosition(_interactedTile.Index);

                        _interactedTile = null;
                    }
                    break;
            }
        }
#endif

    }

    #endregion

    #region Game / Utility methods

    public void InitializeGrid()
    {
        //Reset tiles index, number and position
        //For this purpose, the grid is considered like a long array, merging the matrix rows, starting from the top one
        //i.e tile in the top left corner will have index 0
        for (int i = 0; i < _tiles.Count; i++)
        {
            _tiles[i].Initialize(i, i + 1, IndexToPosition(i));
        }
        _emptyIndex = (GridSideSize * GridSideSize) - 1;

        //shuffle tiles numbers
        for (int i = 0; i < _tiles.Count - 1; i++)
        {
            int j = Random.Range(i, _tiles.Count - 1);
            int temp = _tiles[i].Number;
            _tiles[i].Number = _tiles[j].Number;
            _tiles[j].Number = temp;
        }

        NumberOfMoves = 0;
        Timer = 0;
        VictoryPanel.SetActive(false);
        _gameWon = false;

        //Check if starting instance is not solvable or already a solution; if so, initialize again
        if (!IsSolvable(_tiles) || GameWon())
        {
            InitializeGrid();
        }
    }

    //Returns true if current tile configuration is correct
    private bool GameWon()
    {
        foreach (var tile in _tiles)
        {
            if (!tile.IsCorrect()) return false;
        }

        return true;
    }

    //Convert index of the tile to world position in the grid
    private Vector3 IndexToPosition(int index)
    {
        float x = - (GridSideSize - 1) * 0.5f + (index % GridSideSize);
        float y = (GridSideSize - 1) * 0.5f - (int) (index / GridSideSize);
        return new Vector3(x,y);
    }

    //Check if a tile has an adjacent empty cell
    private bool TileCanMove(Tile tile)
    {
        return Vector2.SqrMagnitude(IndexToPosition(tile.Index) - IndexToPosition(_emptyIndex)) <= 1;
    }

    //Confirm the move logically, swapping indices with the empty space
    private void MoveTile(Tile tile)
    {
        //Swap index with empty space
        int temp = tile.Index;
        tile.Index = _emptyIndex;
        _emptyIndex = temp;

        NumberOfMoves++;

        //Victory check
        if (GameWon())
        {
            VictoryPanel.SetActive(true);
            _gameWon = true;
        }
    }

    // Returns true if given starting instance of N*N - 1 puzzle is solvable (assuming empty space in index 15)
    private bool IsSolvable(List<Tile> tiles)
    {
        var array = tiles.Select(t => t.Number).Append(0).ToArray();
        // Count inversions in given puzzle 
        int invCount = GetInvCount(array);

        return invCount % 2 == 0;
    }

    // Utility function to count inversions in given array. 
    private int GetInvCount(int[] arr)
    {
        int inv_count = 0;
        for (int i = 0; i < GridSideSize * GridSideSize - 1; i++)
        {
            for (int j = i + 1; j < GridSideSize * GridSideSize; j++)
            {
                // count pairs(i, j) such that i appears 
                // before j, but i > j. 
                if (arr[j] != 0 && arr[i] != 0 && arr[i] > arr[j])
                    inv_count++;
            }
        }
        return inv_count;
    }

#endregion


}
