﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [NonSerialized]
    public int Index;

    public int Number
    {
        get { return _number; }
        set
        {
            _number = value;
            _text.text = value.ToString();
        }
    }

    private int _number;

    private TextMeshPro _text;
    private SpriteRenderer _renderer;
    public Sprite TileIdle;
    public Sprite TileActive;

    public enum TileState
    {
        Idle,
        Active,
        CantMove
    }

    public TileState State
    {
        get { return _state; }
        set
        {
            _state = value;
            switch (value)
            {
                case TileState.Idle:
                    _renderer.sprite = TileIdle;
                    _renderer.color = Color.white;
                    break;
                case TileState.Active:
                    _renderer.sprite = TileActive;
                    _renderer.color = Color.white;
                    break;
                case TileState.CantMove:
                    _renderer.sprite = TileActive;
                    _renderer.color = Color.red;
                    break;
            }
        }

    }
    private TileState _state;

    private void Awake()
    {
        _text = GetComponentInChildren<TextMeshPro>();
        _renderer = GetComponentInChildren<SpriteRenderer>();
        State = TileState.Idle;
        _text.text = "";
    }

    public void Initialize(int index, int number, Vector3 position)
    {
        Index = index;
        Number = number;
        transform.position = position;
        State = TileState.Idle;
    }

    public bool IsCorrect()
    {
        return Number == (Index + 1);
    }


}
